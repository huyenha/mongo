# chạy trực tiếp
# Run directly

    docker run -it -e DB_NAME=users -e DB_USER=customuser -e DB_PASS=password -e MONGO_INITDB_ROOT_USERNAME=root -e MONGO_INITDB_ROOT_PASSWORD=123456 -p 27017:27017 --name mongo_cont registry.gitlab.com/huyenha/mongo:latest

# Chạy qua yml
# yml config example

    authdb:
        image: registry.gitlab.com/huyenha/mongo:latest
        networks:
            - auth
        ports:
            - 27017
        restart: always
        environment:
            DB_USER: users
            DB_PASS: password
            DB_NAME: customuser
            MONGO_INITDB_ROOT_USERNAME: root
            MONGO_INITDB_ROOT_PASSWORD: 123456
        volumes:
            - /etc/localtime:/etc/localtime:ro
            - /auth_db:/data/db
