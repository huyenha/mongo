FROM mongo:4.0.10
ADD mongo_entrypoint.sh /
RUN chmod +x /mongo_entrypoint.sh
ENTRYPOINT [ "/mongo_entrypoint.sh" ]
CMD [ "mongod" ]